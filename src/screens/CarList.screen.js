import React, {useState} from 'react';
import {View, Text, Button, Image, Dimensions} from 'react-native';
import {dummyCars} from '../dummyDatas/cars';

const {width, height} = Dimensions.get('window');

const DescriptionRow = (props) => {
  const {fieldName, value} = props;
  return (
    <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
      <View>
        <Text>{fieldName}: </Text>
      </View>
      <View>
        <Text>{value}</Text>
      </View>
    </View>
  );
};

export const CarList = () => {
  const [carIdx, setCarIdx] = useState(0);

  const handleNextCar = () => {
    const totalCars = dummyCars.length;
    const nextIdx = carIdx === totalCars - 1 ? 0 : carIdx + 1;

    setCarIdx(nextIdx);
  };

  const handlePrevCar = () => {
    const totalCars = dummyCars.length;
    const prevIdx = carIdx === 0 ? totalCars - 1 : carIdx - 1;

    setCarIdx(prevIdx);
  };

  const selectedCar = dummyCars[carIdx];

  return (
    <View style={{flex: 1}}>
      <Image
        source={selectedCar.image}
        style={{width, height: 200}}
        resizeMode={'contain'}
      />
      <View style={{paddingHorizontal: width * 0.1, paddingVertical: 18}}>
        <DescriptionRow fieldName="Name" value={dummyCars[carIdx].name} />
        <DescriptionRow fieldName="Brand" value={dummyCars[carIdx].brand} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Button title="Prev" onPress={handlePrevCar} />
        <Button title="Next" onPress={handleNextCar} />
      </View>
    </View>
  );
};

export default CarList;
